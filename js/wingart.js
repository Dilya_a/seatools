// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var tabsBox = $(".tabs"),
	navBtn  = $("#toggle_nav_btn"),
	navOverlay = $(".nav_overlay"),
  owl        = $(".owl_carousel"),
  scroll     = $(".scroll")
	stylerElem = $(".styler");

if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}
if (stylerElem.length){
	include("js/jquery.formstyler.js");
}
if (owl.length){
  include("js/owl.carousel.js");
}

if (scroll.length){
  include("js/jquery.custom-scrollbar.js");
}

// if($(".data-mh").length){
//   include("js/jquery.matchHeight-min.js");
// }
// Плагин скриптом задает высоту блоку и выставляет по самому большому , нужно чтобы на блоке висел класс (data-mh) и атрибут (data-mh) с одним названием для одинаковых блоков, тогда он будет работать, выше находиться его инициализация, еще он хорошо работает при респонзиве. 

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}

// var wingart = {
// 	init : function(val){
// 		if (val == "carousel"){

// 		}
// 	}
// }


// wingart.init();




$(document).ready(function(){

  $("#toggle_nav_btn, .nav_overlay").on("click", function(){
  	$("body").toggleClass("navTrue");
  })

  if(tabsBox.length){
  	tabsBox.each(function(){
  		$(this).easyResponsiveTabs();
  	})
  }

  if(stylerElem.length){
  	stylerElem.styler();
  }

  /* ------------------------------------------------
  OWL START
  ------------------------------------------------ */

    if(owl.length){
        

      owl.owlCarousel({
      navigation : true,
      slideSpeed : 500,
          pagination : true,
      paginationSpeed : 1500,
      autoPlay: 9000,
      stopOnHover: true,
      singleItem:false,
      items: 1,
          navigationText: [ '', '' ],
          itemsDesktop : [1900, 1],
          itemsDesktopSmall : [1080, 1],
          itemsTablet : [768, 1],
          itemsTabletSmall : [600, 1],
          itemsMobile : [479, 1]
      });
    
      }

  /* ------------------------------------------------
  OWL END
  ------------------------------------------------ */



  /* ------------------------------------------------
  SCROLL START
  ------------------------------------------------ */

  $(document).ready(function() {
    $(".scroll").customScrollbar();
  });

  /* ------------------------------------------------
  SCROLL END
  ------------------------------------------------ */


})